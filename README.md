# 【Maixduino 用户库】文字显示
## 概述：

本用户库可实现让maixduino驱动LCD显示中文字模及以图片的形式显示英文

![](./micropython/_images/featured.png)

## 积木

![](./micropython/_images/blocks.png)

### 初始化LCD屏幕显示

![](./micropython/_images/6.png)

### 以图形形式显示英文

![](./micropython/_images/9.png)

**参数说明：**

- 第一个参数： 英文文本
- 第二个参数X： 显示字模距离远点的X坐标
- 第三个参数Y： 显示字模距离远点的Y坐标
- 第四个参数scale: 字体显示大小， 范围在1~4
- 第六个参数颜色： 设置字体颜色

### 加载字库文件(推荐使用)

![](./micropython/_images/10.png)

**参数说明：**

- 第一个参数W：我们用字库工具设置的字体的宽度
- 第二个参数H：我们用字库工具设置的字体的高度
- 第三个参数路径：加载字库路径

如何导出一个字库我会在下面详细说明！

### 以字库形式显示文字

![](./micropython/_images/11.png)

- 第一个参数文字：这里我们用一个变量来引入 变量命名格式为：变量名=b'你想要输入的中文及其他语言'
- 第二个参数X：显示文字距离原点的X坐标
- 第三个参数Y：显示文字距离原点的Y坐标
- 第四个参数间距：显示文字的字符间距
- 第五个参数颜色： 显示文字颜色

### 释放字库

![](./micropython/_images/12.png)

**释放字库后，如果在使用该字库，需要重现加载。**

### 以字模形式显示文字

![](./micropython/_images/7.png)

**参数说明**

- 第一个参数my Variable：字模字符串变量：我们可以开开头声明一个变量= b' 16进制字符串'
- 第二个参数X： 显示字模距离原点的X坐标
- 第三个参数Y： 显示字模距离原点的Y坐标
- 第四个参数W：字模生成设置字体的宽度
- 第五个参数H：字模生成设置字体的高度
- 第六个参数scale: 字体显示大小， 范围在1~4
- 第七个参数颜色： 设置字体颜色

### 显示生效

![](./micropython/_images/13.png)

将我们设置文字形式显示在LCD屏幕上。

### 清屏

![](./micropython/_images/14.png)

## 字库工具

我们将使用tools下的 [FontGenerator.zip]导出字体对应的字库，请看下图完成导出操作。

1. 选择字库编码类型为 Unicode 编码，这将支持绝大多数国家的语言。

   ![](./micropython/_images/15.png)

2. 选择扫描模式，为 5 横向、先上下后左右的字模的扫描打印方向。

   ![](./micropython/_images/16.png)

3. 如下图配置所需要的字体样式后创建字库。

   ![](./micropython/_images/17.png)

4. 保存成 DZK 格式即可，字模数据访问方法如图文字说明

   ![](./micropython/_images/18.png)

## 字模工具

使用目录下的 PCtoLCD.rar软件获取字模的字符串。

1、确认软件为字符模式。

![](./micropython/_images/1.png)

![](./micropython/_images/2.png)

2、点击设置如图即可导出想要的字符串。

![](./micropython/_images/3.png)

3、提取字模字符串即可使用。

![](./micropython/_images/4.png)

```
 这(0) 是(1) 测(2) 试(3)

/x00/x20/x10/x17/x00/x02/xF1/x10/x10/x10/x11/x12/x14/x28/x47/x00/x80/x40/x40/xFC/x10/x10/x20/xA0/x40/xA0/x10/x08/x08/x00/xFE/x00这0
/x1F/x10/x10/x1F/x10/x10/x1F/x00/xFF/x01/x11/x11/x11/x29/x45/x83/xF0/x10/x10/xF0/x10/x10/xF0/x00/xFE/x00/x00/xF8/x00/x00/x00/xFE是1
/x00/x27/x14/x14/x85/x45/x45/x15/x15/x25/xE5/x21/x22/x22/x24/x08/x04/xC4/x44/x54/x54/x54/x54/x54/x54/x54/x54/x04/x84/x44/x14/x08测2
/x00/x20/x10/x10/x07/x00/xF0/x17/x11/x11/x11/x15/x19/x17/x02/x00/x28/x24/x24/x20/xFE/x20/x20/xE0/x20/x10/x10/x10/xCA/x0A/x06/x02试3
```

可以使用图形模式绘制自己喜欢的字模图形，支持 32 * 32 的图形。

![](./micropython/_images/5.png)

## 程序设计

### 任务一：以图形形式显示英文

![](./micropython/_images/8.png)

**实验效果：**

![](./micropython/_images/10.jpg)

### 任务二：以字库显示文字

![](./micropython/_images/19.png)

**显示效果如下**

![](./micropython/_images/11.jpg)

### 任务三：以字模显示文字


![](./micropython/_images/example.png)

**显示效果如下：**

![](./micropython/_images/6.jpg)




# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|:-----:|-----|
|mpython|||√||



# 更新日志

V0.0.1 基础功能完成

V0.0.2 增加字库操作

