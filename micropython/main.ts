enum LINE {
    //% block="myVariable"
    myVariable
}
enum TYPE{
    //% block="ASCII"
    ASCII,
    //% block="UTF8"
    UTF8
}
//% color="#4169E1" iconWidth=50 iconHeight=40
namespace Show{
    //% block="初始化 LCD 屏幕显示" blockType="command"
   
    export function InitLcd(parameter: any, block: any) {
        
        Generator.addImport("import lcd,image",true);
       
        Generator.addInitHeader(2,`lcd.init()`,true)
        
         
    }

    //% block="以图片形式显示英文[MESS] X:[X] Y:[Y]  scale:[S] 颜色:[COLOR]" blockType="command"
    //% MESS.shadow="string"     MESS.defl="Hockel"
    //% X.shadow="number"     X.defl="60"
    //% Y.shadow="number"     Y.defl="100"
    //% S.shadow="range" S.params.min=1 S.params.max=4  S.defl=1
    //% COLOR.shadow="colorPalette"
    export function Showword(parameter: any, block: any) {
        let mess = parameter.MESS.code; 
        let x = parameter.X.code; 
        let y = parameter.Y.code;
        let s = parameter.S.code;
        let c = parameter.COLOR.code;

        Generator.addInitHeader(1,`img = image.Image()`,true)

        Generator.addCode(`img.draw_string(${x}, ${y},  ${mess}, scale=${s},color = ${c})`);


    }
     //% block="---"
     export function noteSep() {

    }
    
    //% block="加载字库 字体 W:[W] H:[H]  路径:[ROUTE]" blockType="command"
    //% W.shadow="number" W.defl="16"
    //% H.shadow="number" H.defl="16"
    //% ROUTE.shadow="string" ROUTE.defl="/sd/0xA00000_font_uincode_16_16_tblr.Dzk"
    export function LoadWordType(parameter: any, block: any) {

        let route = parameter.ROUTE.code; 
        let w = parameter.W.code;
        let h = parameter.H.code;
            
       Generator.addInitHeader(1,`img = image.Image()`,true)

       Generator.addCode(`image.font_load(image.UTF8, ${w}, ${h}, ${route})`);

   
    
   }

    ////% block="以字库形式显示 文字[MESS] X:[X] Y:[Y] 字符间距:[X_SPACE] 顔色[COLOR]" blockType="command"
 
    //% MESS.shadow="dropdownRound"   MESS.options="LINE"     MESS.defl="LINE.myVariable"
    //% X.shadow="number" X.defl="10"
    //% Y.shadow="number" Y.defl="20"
    
    //% X_SPACE.shadow="number" X_SPACE.defl="8"
    //% COLOR.shadow="colorPalette" 
    export function ShowWordTYPE(parameter: any, block: any) {
        let mess = parameter.MESS.code; 
        let x = parameter.X.code; 
        let y = parameter.Y.code;
        
        let x_space = parameter.X_SPACE.code;
        
        let c = parameter.COLOR.code;
   
   

       Generator.addCode(`img.draw_string(${x}, ${y}, ${mess}, x_spacing=${x_space}, mono_space=1, color=${c})`);

   
    
   }
    //% block="释放字库" blockType="command"
   
    export function FreeWordType(parameter: any, block: any) {
        

       Generator.addCode(`image.font_free()`);

   
    
   }
    //% block="---"
    export function noteSep1() {

    }

    //% block="以字模形式显示 文字[MESS] X:[X] Y:[Y] W:[W] H[H] scale:[S] 顔色[COLOR]" blockType="command"
    //% COLOR.shadow="colorPalette" 
    //% MESS.shadow="dropdownRound"   MESS.options="LINE"     MESS.defl="LINE.myVariable"
    //% X.shadow="number" X.defl="10"
    //% Y.shadow="number" Y.defl="20"
    //% W.shadow="number" W.defl="8"
    //% H.shadow="number" H.defl="8"
    //% S.shadow="range" S.params.min=1 S.params.max=4  S.defl=1
    export function ShowChineseWord(parameter: any, block: any) {
         let mess = parameter.MESS.code; 
         let x = parameter.X.code; 
         let y = parameter.Y.code;
         let w = parameter.W.code;
         let h = parameter.H.code;
         let s = parameter.S.code;
         let c = parameter.COLOR.code;
    
       
        
        
        Generator.addInitHeader(1,`img = image.Image()`,true)
    

        Generator.addCode(`img.draw_font(${x}, ${y}, ${w}, ${h}, ${mess}, scale=${s}, color=${c})`);
     
    }

     //% block="---"
    export function noteSep2() {

    }
    
     //% block="显示生效" blockType="command"
   
     export function DisplayEffective(parameter: any, block: any) {
        

        Generator.addCode(`lcd.display(img)`);
 
    
     
    }
    //% block="以[COLOR]清屏 " blockType="command"
    //% COLOR.shadow="colorPalette"
   
    export function ClearDisplay(parameter: any, block: any) {
        
        let c = parameter.COLOR.code;


        Generator.addCode(`lcd.clear(${c})`);
     
         
         
    }
    
   }
    